provider "google" {
  credentials  =  file("CREDENTIALS_FILE.json")
  project      =  "terraform-project-296120"
  region       =  "us-west1"
 
}



// A single Compute Engine instance
resource "google_compute_instance" "test-vm" {
 name         = "test-vm"
 machine_type = "f1-micro"
 zone         = "us-west1-a"

 boot_disk {
   initialize_params {
     image = "debian-cloud/debian-9"
   }
 }

 network_interface {
   network = "default"
 }
}

