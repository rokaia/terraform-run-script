# terraform-run-script
This repo contains:
- example terraform file wich use Google provider to run vm compute instance with network and vpc.
- pipeline with 
  - before stages: to initialize and prepare our work.
  - three stage:  
    - validate: to validate the configuration is valid or not.
    - plan: to plan our terraform script. and we have artifact to save the output for the plan. 
    - apply: to apply the resource written in the file.
    

